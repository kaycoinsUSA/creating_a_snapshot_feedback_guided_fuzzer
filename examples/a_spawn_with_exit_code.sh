#!/usr/bin/env bash

if [ $# -lt 1 ] ; then
    echo "USAGE: $0 CMD [CMD_ARG1] [CMD_ARG2] ..."
    exit 1
fi

echo "Running process '$@'"

"$@"
exit_code=$?

if [ $exit_code != 0 ] ; then
    echo "CRASH!"
fi
